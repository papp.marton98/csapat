package hu.alkfejl.dao;

import hu.alkfejl.model.Player;
import hu.alkfejl.model.User;
import java.util.List;

public interface PlayerDAO {

    List<Player> findAll();
    List<Player> findAll(User user);
    Player findById(int contactId);

    Player save(Player player);

    void delete(Player player);
    void delete(int playerId);

}

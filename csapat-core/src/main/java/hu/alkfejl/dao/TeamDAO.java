package hu.alkfejl.dao;

import hu.alkfejl.model.Team;
import hu.alkfejl.model.User;

import java.util.List;

public interface TeamDAO {

    List<Team> findAll();
    List<Team> findAll(User user);

    List<Team> findAllByPlayerId(int id);
    Team findById(int playerId);

    Team save(Team team, int id);
    Team save(Team team);

    void delete(Team team);
    void delete(int teamId);
    void deleteAll(int id);

}
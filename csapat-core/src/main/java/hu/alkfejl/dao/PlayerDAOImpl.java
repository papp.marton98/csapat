package hu.alkfejl.dao;

import hu.alkfejl.config.PlayerConfiguration;
import hu.alkfejl.model.Player;
import hu.alkfejl.model.User;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PlayerDAOImpl implements PlayerDAO{

    private static final String SELECT_ALL_PLAYERS = "SELECT * FROM PLAYER";
    private static final String INSERT_PLAYER = "INSERT INTO PLAYER (name,  dateOfBirth,  position) VALUES (?,?,?)";
    private static final String UPDATE_PLAYER = "UPDATE PLAYER SET name=?, dateOfBirth=?, position = ? WHERE id=?";
    private static final String DELETE_PLAYER = "DELETE FROM PLAYER WHERE id = ?";
    private static final String SELECT_ALL_PLAYERS_BY_USER = "SELECT * FROM PLAYER WHERE user_id = ?";

    private String connectionURL;

    private static PlayerDAO instance;
    private UserDAO userDAO = UserDAOImpl.getInstance();

    public PlayerDAOImpl() {
        connectionURL = PlayerConfiguration.getValue("db.url");
    }

    @Override
    public List<Player> findAll() {

        List<Player> result = new ArrayList<>();

        try(Connection c = DriverManager.getConnection(connectionURL);
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_ALL_PLAYERS)
        ){

            while(rs.next()){
                Player player = new Player();
                player.setId(rs.getInt("id"));
                player.setName(rs.getString("name"));
                player.setDateOfBirth(rs.getInt("dateOfBirth"));
                player.setPosition(rs.getString("position"));

                result.add(player);

            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;

    }
    @Override
    public List<Player> findAll(User user) {

        List<Player> result = new ArrayList<>();

        try(Connection c = DriverManager.getConnection(connectionURL);
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_ALL_PLAYERS)
        ){

            while(rs.next()){
                Player player = new Player();
                player.setId(rs.getInt("id"));
                player.setName(rs.getString("name"));
                player.setDateOfBirth(rs.getInt("dateOfBirth"));
                player.setPosition(rs.getString("position"));

                result.add(player);

            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;

    }


    public static PlayerDAO getInstance() {
        if(instance == null){
            instance = new PlayerDAOImpl();
        }
        return instance;
    }

    @Override
    public Player save(Player player) {
        try(Connection c = DriverManager.getConnection(connectionURL);
            PreparedStatement stmt = player.getId() <= 0 ? c.prepareStatement(INSERT_PLAYER, Statement.RETURN_GENERATED_KEYS) : c.prepareStatement(UPDATE_PLAYER)
        ){
            if(player.getId() > 0){ // UPDATE
                stmt.setInt(4, player.getId());
            }

            stmt.setString(1, player.getName());
            stmt.setInt(2, player.getDateOfBirth());
            stmt.setString(3, player.getPosition());

            int affectedRows = stmt.executeUpdate();
            if(affectedRows == 0){
                return null;
            }

            if(player.getId() <= 0){ // INSERT
                ResultSet genKeys = stmt.getGeneratedKeys();
                if(genKeys.next()){
                    player.setId(genKeys.getInt(1));
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }

        return player;
    }

    @Override
    public void delete(Player player) {

        try(Connection c = DriverManager.getConnection(connectionURL);
            PreparedStatement stmt = c.prepareStatement(DELETE_PLAYER);
        ) {
            stmt.setInt(1, player.getId());
            stmt.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    @Override
    public void delete(int playerId) {
        try(Connection c = DriverManager.getConnection(connectionURL);
            PreparedStatement stmt = c.prepareStatement(DELETE_PLAYER);
        ) {
            //phoneDAO.deleteAll(playerId);
            stmt.setInt(1, playerId);
            stmt.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public Player findById(int playerId) {
        try (Connection conn = DriverManager.getConnection(connectionURL);
             PreparedStatement pst = conn.prepareStatement("SELECT * FROM PLAYER WHERE id = ?")
        ) {
            pst.setInt(1, playerId);

            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                return extractPlayerFromResultSet(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    Player extractPlayerFromResultSet(ResultSet rs){
        try {
            Player player = new Player();
            player.setId(rs.getInt("id"));
            player.setName(rs.getString("name"));
            player.setDateOfBirth((rs.getInt("dateOfBirth")));
            player.setPosition(rs.getString("position"));

            player.setUser(userDAO.getUserById(rs.getInt("user_id")));
            return player;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}

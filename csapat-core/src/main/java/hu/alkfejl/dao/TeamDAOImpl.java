package hu.alkfejl.dao;


import hu.alkfejl.config.TeamConfiguration;
import hu.alkfejl.model.Team;
import hu.alkfejl.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TeamDAOImpl implements TeamDAO {
    private static final String SELECT_ALL_TEAMS = "SELECT * FROM TEAM";
    private static final String SELECT_TEAMS_BY_PLAYER_ID = "SELECT * FROM TEAM WHERE id=?";
    private static final String INSERT_TEAM = "INSERT INTO TEAM (team_name,  nation, trophies) VALUES (?,?,?)";
    private static final String UPDATE_TEAM = "UPDATE TEAM SET team_name=?, nation = ?, trophies = ? WHERE id=?";
    private static final String DELETE_TEAM = "DELETE FROM TEAM WHERE id = ?";

    private static TeamDAO instance;
    private UserDAO userDAO = UserDAOImpl.getInstance();


    private String connectionURL;

    public TeamDAOImpl() {
        connectionURL = TeamConfiguration.getValue("db.url");
    }


    public static TeamDAO getInstance() {
        if(instance == null){
            instance = new TeamDAOImpl();
        }
        return instance;
    }


    @Override
    public List<Team> findAll() {

        List<Team> result = new ArrayList<>();

        try(Connection c = DriverManager.getConnection(connectionURL);
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_ALL_TEAMS)
        ){

            while(rs.next()){
                Team team = new Team();
                team.setId(rs.getInt("id"));
                team.setTeam_name(rs.getString("team_name"));
                team.setNation(rs.getString("nation"));
                team.setTrophies(rs.getString("trophies"));

                result.add(team);

            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;

    }

    @Override
    public Team save(Team team, int id) {
        try(Connection c = DriverManager.getConnection(connectionURL);
            PreparedStatement stmt = team.getId() <= 0 ? c.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS) : c.prepareStatement(UPDATE_TEAM)
        ){
            if(team.getId() > 0){ // UPDATE
                stmt.setInt(4, team.getId());
            }

            stmt.setString(1, team.getTeam_name());
            stmt.setString(2, team.getNation());
            stmt.setString(3, team.getTrophies());

            int affectedRows = stmt.executeUpdate();
            if(affectedRows == 0){
                return null;
            }

            if(team.getId() <= 0){ // INSERT
                ResultSet genKeys = stmt.getGeneratedKeys();
                if(genKeys.next()){
                    team.setId(genKeys.getInt(1));
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }

        return team;
    }
    @Override
    public Team save(Team team) {
        try(Connection c = DriverManager.getConnection(connectionURL);
            PreparedStatement stmt = team.getId() <= 0 ? c.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS) : c.prepareStatement(UPDATE_TEAM)
        ){
            if(team.getId() > 0){ // UPDATE
                stmt.setInt(4, team.getId());
            }

            stmt.setString(1, team.getTeam_name());
            stmt.setString(2, team.getNation());
            stmt.setString(3, team.getTrophies());

            int affectedRows = stmt.executeUpdate();
            if(affectedRows == 0){
                return null;
            }

            if(team.getId() <= 0){ // INSERT
                ResultSet genKeys = stmt.getGeneratedKeys();
                if(genKeys.next()){
                    team.setId(genKeys.getInt(1));
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }

        return team;
    }

    @Override
    public void delete(Team team) {

        try(Connection c = DriverManager.getConnection(connectionURL);
            PreparedStatement stmt = c.prepareStatement(DELETE_TEAM);
        ) {
            stmt.setInt(1, team.getId());
            stmt.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
    @Override
    public void delete(int teamId) {
        try(Connection c = DriverManager.getConnection(connectionURL);
            PreparedStatement stmt = c.prepareStatement(DELETE_TEAM);
        ) {
            stmt.setInt(1, teamId);
            stmt.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public Team findById(int teamId) {
        try (Connection conn = DriverManager.getConnection(connectionURL);
             PreparedStatement pst = conn.prepareStatement("SELECT * FROM TEAM WHERE id = ?")
        ) {
            pst.setInt(1, teamId);

            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                return extractTeamFromResultSet(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    Team extractTeamFromResultSet(ResultSet rs){
        try {
            Team team = new Team();
            team.setId(rs.getInt("id"));
            team.setTeam_name(rs.getString("team_name"));
            team.setNation(rs.getString("nation"));
            team.setTrophies(rs.getString("trophies"));

            team.setUser(userDAO.getUserById(rs.getInt("user_id")));
            return team;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Team> findAllByPlayerId(int playerId) {
        List<Team> result = new ArrayList<>();

        try(Connection c = DriverManager.getConnection(connectionURL);
            PreparedStatement statement = c.prepareStatement(SELECT_TEAMS_BY_PLAYER_ID)){

            statement.setInt(1, playerId);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Team team = new Team();
                team.setId(rs.getInt("id"));
                team.setTeam_name(rs.getString("team_name"));
                team.setNation(rs.getString("nation"));
                team.setTrophies(rs.getString("trophies"));
                //int ordinalValue = rs.getInt("phoneType");


                result.add(team);

            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Team> findAll(User user) {

        List<Team> result = new ArrayList<>();

        try(Connection c = DriverManager.getConnection(connectionURL);
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_ALL_TEAMS)
        ){

            while(rs.next()){
                Team team = new Team();
                team.setId(rs.getInt("id"));
                team.setTeam_name(rs.getString("team_name"));
                team.setNation(rs.getString("nation"));
                team.setTrophies(rs.getString("trophies"));

                result.add(team);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;

    }
    @Override
    public void deleteAll(int id) {

    }

}

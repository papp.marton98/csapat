package hu.alkfejl.model;

import javafx.beans.property.*;
import javafx.collections.ObservableList;

public class Player {

    private IntegerProperty id = new SimpleIntegerProperty(this, "id");
    private StringProperty name = new SimpleStringProperty(this, "name");
    private ObjectProperty<ObservableList<Team>> teams = new SimpleObjectProperty<>(this, "teams");
    private IntegerProperty dateOfBirth = new SimpleIntegerProperty(this, "dateOfBirth");
    private StringProperty position = new SimpleStringProperty(this, "position");
    private ObjectProperty<User> user = new SimpleObjectProperty<>(this, "user");


    public int getId() {
        return id.get();
    }
    public IntegerProperty idProperty() {
        return id;
    }
    public void setId(int id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }
    public StringProperty nameProperty() {
        return name;
    }
    public void setName(String name) {
        this.name.set(name);
    }

    public void setUser(User user) { this.user.set(user);}

    public ObservableList<Team> getTeams() {
        return teams.get();
    }
    public ObjectProperty<ObservableList<Team>> teamsProperty() {
        return teams;
    }
    public void setTeams(ObservableList<Team> teams) {
        this.teams.set(teams);
    }

    public int getDateOfBirth() {
        return dateOfBirth.get();
    }
    public IntegerProperty dateOfBirthProperty() {
        return dateOfBirth;
    }

    public void setDateOfBirth(int dateOfBirth) {
        this.dateOfBirth.set(dateOfBirth);
    }
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth.set(Integer.parseInt(dateOfBirth));
    }

    public String getPosition() {
        return position.get();
    }
    public StringProperty positionProperty() {
        return position;
    }
    public void setPosition(String position) {
        this.position.set(position);
    }
}

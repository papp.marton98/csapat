package hu.alkfejl.model;

import javafx.beans.property.*;

public class Team {

    private IntegerProperty id = new SimpleIntegerProperty(this, "id");
    private StringProperty team_name = new SimpleStringProperty(this, "team_name");
    private StringProperty nation = new SimpleStringProperty(this, "nation");
    private StringProperty trophies = new SimpleStringProperty(this, "trophies");
    private ObjectProperty<User> user = new SimpleObjectProperty<>(this, "user");


    public int getId() {
        return id.get();
    }
    public IntegerProperty idProperty() {
        return id;
    }
    public void setId(int id) {
        this.id.set(id);
    }

    public void setUser(User user) {
        this.user.set(user);
    }

    public String getTeam_name() {
        return team_name.get();
    }
    public StringProperty team_nameProperty() {
        return team_name;
    }
    public void setTeam_name(String team_name) {
        this.team_name.set(team_name);
    }

    public String getNation() {
        return nation.get();
    }
    public StringProperty nationProperty() {
        return nation;
    }
    public void setNation(String nation) {
        this.nation.set(nation);
    }

    public void setTrophies(String trophies) {
        this.trophies.set(trophies);
    }
    public String getTrophies() {
        return trophies.get();
    }
    public StringProperty trophiesProperty() {
        return trophies;
    }
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:basic-layout-menu title="Add Player">
    <jsp:attribute name="header">
            <script src="${pageContext.request.contextPath}/js/add-player.js"></script>
    </jsp:attribute>
    <jsp:body>
        <jsp:useBean id="player" class="hu.alkfejl.model.Player" scope="request"/>

        <form action="${pageContext.request.contextPath}/PlayerController" method="post">
            <input type="hidden" name="id" value="${player.id}"/>
            <div class="form-group">
                <label for="name">Name</label>
                <input required name="name" type="text" class="form-control" id="name"
                       placeholder="Enter name" value="${player.name}"/>
            </div>

            <div class="form-group">
                <label for="dateOfBirth">Birthday</label>
                <input id="dateOfBirth" name="dateOfBirth" type="text" class="form-control" id="dateOfBirth"
                       placeholder="Date of Birth" value="${player.dateOfBirth}"/>
            </div>

            <div class="form-group">
                <label for="position">Position</label>
                <input id="position" name="position" type="text" class="form-control" id="position"
                       placeholder="Position" value="${player.position}"/>
            </div>
            <button id="submit" type="submit" class="btn btn-primary">Submit</button>
        </form>
    </jsp:body>
</t:basic-layout-menu>



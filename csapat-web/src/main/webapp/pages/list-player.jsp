<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<jsp:include page="/PlayerController"/>

<t:basic-layout-menu title="List Players">
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Birth</th>
            <th scope="col">Position</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="item" items="${requestScope.playerList}">
            <tr>
                <td>${item.name}</td>
                <td>${item.dateOfBirth}</td>
                <td>${item.position}</td>
                <td>
                    <a href="../UpdatePlayerController?playerId=${item.id}"><i class="fas fa-edit"></i></a>
                    <a href="../DeletePlayerController?playerId=${item.id}"><i class="fas fa-trash"></i></a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</t:basic-layout-menu>





<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<jsp:include page="/TeamController"/>

<t:basic-layout-menu title="List Teams">
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Nation</th>
            <th scope="col">Trophies</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="item" items="${requestScope.teamList}">
            <tr>
                <td>${item.team_name}</td>
                <td>${item.nation}</td>
                <td>${item.trophies}</td>
                <td>
                    <a href="../UpdateTeamController?teamId=${item.id}"><i class="fas fa-edit"></i></a>
                    <a href="../DeleteTeamController?teamId=${item.id}"><i class="fas fa-trash"></i></a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</t:basic-layout-menu>





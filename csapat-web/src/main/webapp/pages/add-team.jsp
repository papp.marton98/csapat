<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:basic-layout-menu title="Add Team">
    <jsp:attribute name="header">
            <script src="${pageContext.request.contextPath}/js/add-team.js"></script>
    </jsp:attribute>
    <jsp:body>
        <jsp:useBean id="team" class="hu.alkfejl.model.Team" scope="request"/>

        <form action="${pageContext.request.contextPath}/TeamController" method="post">
            <input type="hidden" name="id" value="${team.id}"/>
            <div class="form-group">
                <label for="team_name">Name</label>
                <input required name="team_name" type="text" class="form-control" id="team_name"
                       placeholder="Enter name" value="${team.team_name}"/>
            </div>

            <div class="form-group">
                <label for="nation">Nation</label>
                <input id="nation" name="nation" type="text" class="form-control" id="nation"
                       placeholder="Nation" value="${team.nation}"/>
            </div>

            <div class="form-group">
                <label for="trophies">Trophies</label>
                <input id="trophies" name="position" type="text" class="form-control" id="trophies"
                       placeholder="Trophies" value="${team.trophies}"/>
            </div>
            <button id="submit" type="submit" class="btn btn-primary">Submit</button>
        </form>
    </jsp:body>
</t:basic-layout-menu>



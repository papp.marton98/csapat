package hu.alkfejl.controller;

import hu.alkfejl.dao.PlayerDAO;
import hu.alkfejl.dao.PlayerDAOImpl;
import hu.alkfejl.model.Player;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/UpdatePlayerController")
public class UpdatePlayerController extends HttpServlet {
    private PlayerDAO dao = PlayerDAOImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String playerIdStr = req.getParameter("playerId");

        if(playerIdStr != null && !playerIdStr.isEmpty()){
            int playerId = Integer.parseInt(playerIdStr);
            Player player = dao.findById(playerId);
            req.setAttribute("player", player);
        }

        req.getRequestDispatcher("pages/add-player.jsp").forward(req, resp);
    }
}

package hu.alkfejl.controller;

import hu.alkfejl.dao.PlayerDAO;
import hu.alkfejl.dao.PlayerDAOImpl;
import hu.alkfejl.model.Player;
import hu.alkfejl.model.User;
import java.io.IOException;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/PlayerController")
public class PlayerController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private PlayerDAO dao = PlayerDAOImpl.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        int playerId = 0;
        try {
            playerId = Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException ex){
            ex.printStackTrace();
        }

        Player c = dao.findById(playerId);

        if(c == null){
            c = new Player();
        }

        try {
            c.setName(request.getParameter("name"));
            c.setDateOfBirth(request.getParameter("dateOfBirth"));
            c.setPosition(request.getParameter("position"));

            User currentUser = (User) request.getSession().getAttribute("currentUser");
            c.setUser(currentUser);

            dao.save(c);

            response.sendRedirect("pages/list-player.jsp");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User currentUser = (User) req.getSession().getAttribute("currentUser");
        List<Player> all = dao.findAll(currentUser);
        req.setAttribute("playerList", all);
    }
}

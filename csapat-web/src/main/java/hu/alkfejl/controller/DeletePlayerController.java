package hu.alkfejl.controller;


import hu.alkfejl.dao.PlayerDAO;
import hu.alkfejl.dao.PlayerDAOImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/DeletePlayerController")
public class DeletePlayerController extends HttpServlet {

    PlayerDAO dao = PlayerDAOImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int playerId = Integer.parseInt(req.getParameter("playerId"));
            dao.delete(playerId);
        }
        catch (NumberFormatException ex){
            ex.printStackTrace();
        }

        resp.sendRedirect("pages/list-player.jsp");
    }
}

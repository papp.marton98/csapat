package hu.alkfejl.controller;

import hu.alkfejl.dao.TeamDAO;
import hu.alkfejl.dao.TeamDAOImpl;
import hu.alkfejl.model.Team;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/UpdateTeamController")
public class UpdateTeamController extends HttpServlet {
    private TeamDAO dao = TeamDAOImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String teamIdStr = req.getParameter("teamId");

        if(teamIdStr != null && !teamIdStr.isEmpty()){
            int teamId = Integer.parseInt(teamIdStr);
            Team team = dao.findById(teamId);
            req.setAttribute("team", team);
        }

        req.getRequestDispatcher("pages/add-team.jsp").forward(req, resp);
    }
}

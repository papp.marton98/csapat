package hu.alkfejl.controller;

import hu.alkfejl.dao.TeamDAO;
import hu.alkfejl.dao.TeamDAOImpl;
import hu.alkfejl.model.Team;
import hu.alkfejl.model.User;
import java.io.IOException;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/TeamController")
public class TeamController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private TeamDAO dao = TeamDAOImpl.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        int teamId = 0;
        try {
            teamId = Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException ex){
            ex.printStackTrace();
        }

        Team c = dao.findById(teamId);

        if(c == null){
            c = new Team();
        }

        try {
            c.setTeam_name(request.getParameter("team_name"));
            c.setNation(request.getParameter("nation"));
            c.setTrophies(request.getParameter("trophies"));

            User currentUser = (User) request.getSession().getAttribute("currentUser");
            c.setUser(currentUser);

            dao.save(c);

            response.sendRedirect("pages/list-team.jsp");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User currentUser = (User) req.getSession().getAttribute("currentUser");
        List<Team> all = dao.findAll(currentUser);
        req.setAttribute("teamList", all);
    }
}

package hu.alkfejl.controller;

import hu.alkfejl.dao.TeamDAO;
import hu.alkfejl.dao.TeamDAOImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/DeleteTeamController")
public class DeleteTeamController extends HttpServlet {

    TeamDAO dao = TeamDAOImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int teamId = Integer.parseInt(req.getParameter("teamId"));
            dao.delete(teamId);
        }
        catch (NumberFormatException ex){
            ex.printStackTrace();
        }

        resp.sendRedirect("pages/list-team.jsp");
    }
}

package hu.alkfejl.controller;

import hu.alkfejl.App;
import hu.alkfejl.dao.*;
import hu.alkfejl.model.Player;
import hu.alkfejl.model.Team;
import javafx.beans.binding.StringBinding;
import javafx.beans.binding.When;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.converter.NumberStringConverter;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class AddEditPlayerController implements Initializable {

    private Player player;
    private TeamDAO teamDAO = new TeamDAOImpl();
    private PlayerDAO playerDAO = new PlayerDAOImpl();

    @FXML
    private Button saveBtn;

    @FXML
    private TextField name;

    @FXML
    private TextField dateOfBirth;

    @FXML
    private TextField position;

    @FXML
    ListView<Team> teams;

    @FXML
    private Label nameErrors;


    public void setPlayer(Player c) {
        this.player = c;

        List<Team> teamsList = teamDAO.findAllByPlayerId(c.getId());
        player.setTeams(FXCollections.observableArrayList(teamsList));
        name.textProperty().bindBidirectional(player.nameProperty());
        teams.itemsProperty().bindBidirectional(player.teamsProperty());
        dateOfBirth.textProperty().bindBidirectional(player.dateOfBirthProperty(),  new NumberStringConverter());
        position.textProperty().bindBidirectional(player.positionProperty());
        }

    @FXML
    public void onCancel(){
        App.loadFXML("/fxml/player.fxml");
    }

    @FXML
    public void onSave(){
        player = playerDAO.save(player);
        teamDAO.deleteAll(player.getId());
        player.getTeams().forEach(team -> {
            team.setId(0);
            teamDAO.save(team, player.getId());
        });
        App.loadFXML("/fxml/player.fxml");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        saveBtn.disableProperty().bind(name.textProperty().isEmpty()
                        .or(dateOfBirth.textProperty().isEmpty()));

        name.textProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null && newValue.isEmpty()){
                nameErrors.setText("Name is required");
            }
            else{
                nameErrors.setText("");
            }
        });


        teams.setCellFactory(param -> {
            ListCell<Team> cell = new ListCell<>();
            ContextMenu contextMenu = new ContextMenu();

            MenuItem editItem = new MenuItem("Edit");
            MenuItem deleteItem = new MenuItem("Delete");

            contextMenu.getItems().addAll(editItem, deleteItem);

            editItem.setOnAction(event -> {
                Team item = cell.getItem();
                showTeamDialog(item);
            });
            deleteItem.setOnAction(event -> {
                player.getTeams().remove(cell.getItem());
            });

            StringBinding cellTextBinding = new When(cell.itemProperty().isNotNull()).then(cell.itemProperty().asString()).otherwise("");
            cell.textProperty().bind(cellTextBinding);

            cell.emptyProperty().addListener((observable, wasEmpty, isNowEmpty) -> {
                if(isNowEmpty){
                    cell.setContextMenu(null);
                } else{
                    cell.setContextMenu(contextMenu);
                }

            });
            return cell;

        });
    }


    private void showTeamDialog(Team item){
        showTeamDialog(new Team());
    }
}

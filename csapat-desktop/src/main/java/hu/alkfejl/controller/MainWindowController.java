package hu.alkfejl.controller;


import hu.alkfejl.App;
import hu.alkfejl.model.Player;
import hu.alkfejl.model.Team;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

public class MainWindowController  {

    @FXML
    public void onExit(){
        Platform.exit();
    }

    @FXML
    public void onPlayer() {
        App.loadFXML(("/fxml/player.fxml"));
    }

    @FXML
    public void onTeam() {
        App.loadFXML(("/fxml/team.fxml"));
    }

    @FXML
    public void onAddNewPlayer(){
        FXMLLoader fxmlLoader = App.loadFXML(("/fxml/add_edit_player.fxml"));
        AddEditPlayerController controller = fxmlLoader.getController();
        controller.setPlayer(new Player());
    }

    @FXML
    public void onAddNewTeam(){
        FXMLLoader fxmlLoader = App.loadFXML(("/fxml/add_edit_team.fxml"));
        AddEditTeamController controller = fxmlLoader.getController();
        controller.setTeam(new Team());
    }

}


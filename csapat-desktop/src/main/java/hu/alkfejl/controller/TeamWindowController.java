package hu.alkfejl.controller;

import hu.alkfejl.App;
import hu.alkfejl.dao.TeamDAO;
import hu.alkfejl.dao.TeamDAOImpl;
import hu.alkfejl.model.Player;
import hu.alkfejl.model.Team;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class TeamWindowController implements Initializable {
    private TeamDAO dao = new TeamDAOImpl();
    private List<Team> all;

    @FXML
    private TableView<Team> teamTable;

    @FXML
    private TableColumn<Team, String> nameColumn;

    @FXML
    private TableColumn<Team, String> nationColumn;

    @FXML
    private TableColumn<Team, String> trophiesColumn;

    @FXML
    private TableColumn<Team, Void> actionsColumn;

    @FXML
    private TextField nameSearch;

    @FXML
    private TextField nationSearch;

    @FXML
    public void onSearch(){
        List<Team> filtered = all.stream().filter(team -> team.getTeam_name().contains(nameSearch.getText()) && team.getNation().contains(nationSearch.getText())).collect(Collectors.toList());
        teamTable.getItems().setAll(filtered);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshTable();

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("team_name"));
        nationColumn.setCellValueFactory(new PropertyValueFactory<>("nation"));
        trophiesColumn.setCellValueFactory(new PropertyValueFactory<>("trophies"));

        actionsColumn.setCellFactory(param -> new TableCell<>(){

            private final Button deleteBtn = new Button("Delete");
            private final Button editBtn = new Button("Edit");

            {
                deleteBtn.setOnAction(event -> {
                    Team c = getTableRow().getItem();
                    deleteTeam(c);
                    refreshTable();
                });

                editBtn.setOnAction(event -> {
                    Team c = getTableRow().getItem();
                    editTeam(c);
                    refreshTable();
                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                if(empty){
                    setGraphic(null);
                }
                else{
                    HBox container = new HBox();
                    container.getChildren().addAll(editBtn, deleteBtn);
                    container.setSpacing(10.0);
                    setGraphic(container);
                }
            }
        });


    }

    private void editTeam(Team c) {
        FXMLLoader fxmlLoader = App.loadFXML(("/fxml/add_edit_team.fxml"));
        AddEditTeamController controller = fxmlLoader.getController();
        controller.setTeam(c);

    }

    private void deleteTeam(Team c) {
        Alert confirm = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete team: " + c.getTeam_name(), ButtonType.YES, ButtonType.NO);
        confirm.showAndWait().ifPresent(buttonType -> {
            if(buttonType.equals(ButtonType.YES)){
                dao.delete(c);
            }
        });
    }

    private void refreshTable() {
        all = dao.findAll();
        teamTable.getItems().setAll(all);
    }

    @FXML
    public void onExit(){
        Platform.exit();
    }

    @FXML
    public void onAddNewTeam(){
        FXMLLoader fxmlLoader = App.loadFXML(("/fxml/add_edit_team.fxml"));
        AddEditTeamController controller = fxmlLoader.getController();
        controller.setTeam(new Team());
    }

    @FXML
    public void OnMainWindow(){
        App.loadFXML(("/fxml/main_window.fxml"));
    }



    @FXML
    public void onAddNewPlayer(){
        FXMLLoader fxmlLoader = App.loadFXML(("/fxml/add_edit_player.fxml"));
        AddEditPlayerController controller = fxmlLoader.getController();
        controller.setPlayer(new Player());
    }
}

package hu.alkfejl.controller;

import hu.alkfejl.App;
import hu.alkfejl.dao.PlayerDAO;
import hu.alkfejl.dao.PlayerDAOImpl;
import hu.alkfejl.model.Player;
import hu.alkfejl.model.Team;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class PlayerWindowController implements Initializable {

    private PlayerDAO dao = new PlayerDAOImpl();
    private List<Player> all;

    @FXML
    private TableView<Player> playerTable;

    @FXML
    private TableColumn<Player, String> nameColumn;

    @FXML
    private TableColumn<Player, String> positionColumn;

    @FXML
    private TableColumn<Player, String> dateOfBirthColumn;

    @FXML
    private TableColumn<Player, Void> actionsColumn;

    @FXML
    private TextField nameSearch;

    @FXML
    private TextField positionSearch;

    @FXML
    public void onSearch(){
        List<Player> filtered = all.stream().filter(player -> player.getName().contains(nameSearch.getText()) && player.getPosition().contains(positionSearch.getText())).collect(Collectors.toList());
        playerTable.getItems().setAll(filtered);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshTable();

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        positionColumn.setCellValueFactory(new PropertyValueFactory<>("position"));
        dateOfBirthColumn.setCellValueFactory(new PropertyValueFactory<>("dateOfBirth"));

        actionsColumn.setCellFactory(param -> new TableCell<>(){

            private final Button deleteBtn = new Button("Delete");
            private final Button editBtn = new Button("Edit");

            {
                deleteBtn.setOnAction(event -> {
                    Player c = getTableRow().getItem();
                    deletePlayer(c);
                    refreshTable();
                });

                editBtn.setOnAction(event -> {
                    Player c = getTableRow().getItem();
                    editPlayer(c);
                    refreshTable();
                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                if(empty){
                    setGraphic(null);
                }
                else{
                    HBox container = new HBox();
                    container.getChildren().addAll(editBtn, deleteBtn);
                    container.setSpacing(10.0);
                    setGraphic(container);
                }
            }
        });


    }

    private void editPlayer(Player c) {
        FXMLLoader fxmlLoader = App.loadFXML(("/fxml/add_edit_player.fxml"));
        AddEditPlayerController controller = fxmlLoader.getController();
        controller.setPlayer(c);

    }

    private void deletePlayer(Player c) {
        Alert confirm = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete contact: " + c.getName(), ButtonType.YES, ButtonType.NO);
        confirm.showAndWait().ifPresent(buttonType -> {
            if(buttonType.equals(ButtonType.YES)){
                //phoneDAO.deleteAll(c.getId());
                dao.delete(c);
            }
        });
    }



    private void refreshTable() {
        all = dao.findAll();
        playerTable.getItems().setAll(all);
    }


    @FXML
    public void onExit(){
        Platform.exit();
    }

    @FXML
    public void onAddNewPlayer(){
        FXMLLoader fxmlLoader = App.loadFXML(("/fxml/add_edit_player.fxml"));
        AddEditPlayerController controller = fxmlLoader.getController();
        controller.setPlayer(new Player());
    }

    @FXML
    public void OnMainWindow(){
        App.loadFXML(("/fxml/main_window.fxml"));
    }

    @FXML
    public void onAddNewTeam(){
        FXMLLoader fxmlLoader = App.loadFXML(("/fxml/add_edit_team.fxml"));
        AddEditTeamController controller = fxmlLoader.getController();
        controller.setTeam(new Team());
    }
}

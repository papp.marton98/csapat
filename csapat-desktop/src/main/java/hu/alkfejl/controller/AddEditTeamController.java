package hu.alkfejl.controller;


import hu.alkfejl.App;
import hu.alkfejl.dao.*;
import hu.alkfejl.model.Team;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import java.net.URL;
import java.util.ResourceBundle;

public class AddEditTeamController implements Initializable {

    private Team team;
    private TeamDAO teamDAO = new TeamDAOImpl();


    @FXML
    private Button saveBtn;

    @FXML
    private TextField team_name;

    @FXML
    private TextField nation;

    @FXML
    private TextField trophies;

    @FXML
    private Label nameErrors;

    public void setTeam(Team c) {
        this.team = c;

        team_name.textProperty().bindBidirectional(team.team_nameProperty());
        nation.textProperty().bindBidirectional(team.nationProperty());
        trophies.textProperty().bindBidirectional(team.trophiesProperty());
    }

    @FXML
    public void onCancel(){
        App.loadFXML("/fxml/team.fxml");
    }


    @FXML
    public void onSave(){
        team = teamDAO.save(team, team.getId());
        App.loadFXML("/fxml/team.fxml");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        saveBtn.disableProperty().bind(team_name.textProperty().isEmpty()
                .or(nation.textProperty().isEmpty()));

        team_name.textProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null && newValue.isEmpty()){
                nameErrors.setText("Name is required");
            }
            else{
                nameErrors.setText("");
            }
        });
    }


}

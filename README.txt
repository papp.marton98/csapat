Alaklmazásfejlesztés I. Projekt
Papp Márton ZLPQ1L

Választott feladat: Csapat

A feladat egy focicsapat játékosait és meccseit kezelő szoftver kifejlesztése.

A működő funkciók:
-Asztali felület:
    -Játékosok listázása
    -Csapatok listázása
    -Játékos hozzáadása
    -Csapat hozzáadása
    -Játékos módosítása
    -Csapat módosítása
    -Játékos törlése
    -Csapat törlése
    -Játékos keresése név alapján
    -Játékos keresése pozíció alapján
    -Csapat keresése név alapján
    -Csapat keresése nemzetiség alapján
-Webes feület:
    -Játékosok listázása
    -Csapatok listázása
    -Játékos hozzáadása
    -Csapat hozzáadása
    -Játékos módosítása
    -Játékos törlése
    -Csapat törlése

Fordítás és futtatás a projekt fejlesztői környezetbe importálása után:
-Az asztali rész a csapat-desktop modul Plugins/javafx/javafx:run maven futtatásával történik
-A webes rész Tomcat segítségével

A webes-részen az adatbázis elérhető az alma/alma felhasználónév-jelszó párossal, vagy új regisztráció után a választott adatokkal.

Az adatbázis tárolása a csapat.db fájlban történik, a megfelelő elérési utat a csapat-core/src/main/resources/application.properties
fájlban kell beállítani.

